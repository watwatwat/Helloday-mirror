package me.noly.helloday.movie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieControllerTest {

    @Autowired MovieController movieController;

    @Test
    public void getMovie() throws Exception {

        MovieResource recommendation = movieController.getMovie();

        System.out.print(recommendation);
    }
}