package me.noly.helloday.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowCredentials(false).maxAge(3600);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/docs")
                .setViewName("redirect:/docs/");
        registry.addViewController("/docs/")
                .setViewName("forward:/docs/index.html");
        registry.addViewController("/demo")
                .setViewName("redirect:/demo/");
        registry.addViewController("/demo/")
                .setViewName("forward:/demo/index.html");


        super.addViewControllers(registry);
    }
}
