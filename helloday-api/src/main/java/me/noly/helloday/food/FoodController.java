package me.noly.helloday.food;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@Log
public class FoodController {

    private static final String[] TAGS = {
            // matin
            "cacao", "lait",
            // midi
            "riz", "pates", "abricot",
            // soir
            "gratin", "beauf", "burger"
    };

    @GetMapping("/api/foods")
    RecipeResource getFood() throws IOException {

//        ArrayList<Recipe> recipes = Recipe.search(TAGS[new Random().nextInt(TAGS.length)]);
//        Recipe chosenRecipe = recipes.get(new Random().nextInt(recipes.size()));
//        chosenRecipe.loadInformations();

        Recipe randomRecipe = Recipe.getRandomRecipe();

        return new RecipeResource(randomRecipe);
    }
}
