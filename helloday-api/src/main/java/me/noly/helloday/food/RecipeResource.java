package me.noly.helloday.food;

import lombok.Data;

import java.util.List;

@Data
public class RecipeResource {

    private String id;
    private String name;
    private String imageUrl;
    private String description;
    private String cookingTime;
    private String prepareTime;
    private List<Ingredient> ingredients;
    private String url;

    RecipeResource() {
    }

    RecipeResource(Recipe recipe) {
        id = recipe.getId();
        name = recipe.getName();
        imageUrl = recipe.imageUrl;
        description = recipe.getDetails();
        cookingTime = recipe.getCookingTime();
        cookingTime = recipe.getCookingTime();
        ingredients = recipe.getIngredients();
        url = recipe.getUrlLink();
    }
}
