package me.noly.helloday.profile.body;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateProfileBody {

    @NotNull
    private String deviceId;
}
