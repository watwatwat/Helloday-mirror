package me.noly.helloday.profile.service;

import me.noly.helloday.profile.body.CreateProfileBody;
import me.noly.helloday.profile.entity.ProfileEntity;
import me.noly.helloday.profile.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

@Service
@Transactional
public class ProfileService {

    private final ProfileRepository profileRepository;

    @Autowired
    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    public Iterable<ProfileEntity> findProfiles() {

        return profileRepository.findAll();
    }


    public String extractProfileIdFromUrl() {
        return "";
    }

    public ProfileEntity findProfileById(Long id) {

        ProfileEntity profileEntity = profileRepository.findOne(id);
        if (profileEntity == null) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Profile not found");
        }

        return profileEntity;
    }

    public ProfileEntity createProfile(CreateProfileBody body) {
        ProfileEntity profile = new ProfileEntity();
        profile.setDeviceId(body.getDeviceId());
        return profileRepository.save(profile);
    }

    public void removeProfile(Long id) {
        profileRepository.delete(id);
    }
}
