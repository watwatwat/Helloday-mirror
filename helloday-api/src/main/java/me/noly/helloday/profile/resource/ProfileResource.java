package me.noly.helloday.profile.resource;

import lombok.Data;

@Data
public class ProfileResource {

    private Long id;
    private String deviceId;
}
