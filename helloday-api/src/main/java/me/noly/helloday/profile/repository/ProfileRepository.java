package me.noly.helloday.profile.repository;


import me.noly.helloday.profile.entity.ProfileEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<ProfileEntity, Long> {
}
