package me.noly.helloday.profile.assembler;

import me.noly.helloday.profile.entity.ProfileEntity;
import me.noly.helloday.profile.resource.ProfileResource;
import org.springframework.stereotype.Service;

@Service
public class ProfileAssembler {

    public ProfileResource fromEntity(ProfileEntity entity) {
        ProfileResource profileResource = new ProfileResource();

        profileResource.setId(entity.getId());
        profileResource.setDeviceId(entity.getDeviceId());

        return profileResource;
    }
}
