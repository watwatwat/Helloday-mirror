package me.noly.helloday.profile.controller;

import me.noly.helloday.profile.assembler.ProfileAssembler;
import me.noly.helloday.profile.body.CreateProfileBody;
import me.noly.helloday.profile.resource.ProfileResource;
import me.noly.helloday.profile.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProfileController {

    private final ProfileAssembler profileAssembler;

    private final ProfileService profileService;

    @Autowired
    public ProfileController(ProfileAssembler profileAssembler, ProfileService profileService) {
        this.profileAssembler = profileAssembler;
        this.profileService = profileService;
    }

    @GetMapping("/api/test")
    public String getTest() {
        return "Hello World héhé";
    }

    @GetMapping("/api/profiles")
    public List<ProfileResource> getProfiles() {
        ArrayList<ProfileResource> profileResources = new ArrayList<>();
        profileService.findProfiles().forEach(profileEntity ->
                profileResources.add(profileAssembler.fromEntity(profileEntity)));
        return profileResources;
    }

    @GetMapping("/api/profiles/{id}")
    public ProfileResource getProfileById(@PathVariable("id") Long id) {
        return this.profileAssembler.fromEntity(profileService.findProfileById(id));
    }

    @PostMapping("/api/profiles")
    public ProfileResource postProfile(@RequestBody CreateProfileBody body) {
        return this.profileAssembler.fromEntity(profileService.createProfile(body));
    }

    @DeleteMapping("/api/profiles/{id}")
    public ProfileResource deleteProfileById(@PathVariable("id") Long id) {
        return this.profileAssembler.fromEntity(profileService.findProfileById(id));
    }
}
