package me.noly.helloday.movie;

import com.uwetrottmann.tmdb2.Tmdb;
import com.uwetrottmann.tmdb2.entities.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Random;

@RestController
public class MovieController {

    private static final String API_KEY = "1ef7ea4c640a7f9f3c6ba48469cf5a01";

    private Tmdb tmdb = new Tmdb(API_KEY + "\n");

    @Autowired
    private MovieProfileRepository movieProfileRepository;

    @GetMapping("/api/movies")
    MovieResource getMovie() throws IOException {

        // note: that's pure fake
        int page = new Random().nextInt(200) + 1;

        List<Movie> movies = tmdb
                .moviesService()
                .popular(page, "fr")
                .execute()
                .body()
                .results;

        return new MovieResource(movies.get(0));
    }

    @PostMapping("/api/movies/{id}/confirm")
    MovieProfileEntity confirmMovie(@PathVariable() String movieId,
                                    @RequestParam() String profileId) {
        return movieProfileRepository.save(new MovieProfileEntity(profileId, movieId));
    }
}
