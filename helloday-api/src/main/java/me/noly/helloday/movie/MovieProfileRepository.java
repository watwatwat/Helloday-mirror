package me.noly.helloday.movie;

import org.springframework.data.repository.CrudRepository;

public interface MovieProfileRepository extends CrudRepository<MovieProfileEntity, Long> {
}

