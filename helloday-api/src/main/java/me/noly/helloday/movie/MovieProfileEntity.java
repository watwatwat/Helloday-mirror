package me.noly.helloday.movie;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class MovieProfileEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String profileId;
    private String movieId;
    private Boolean accepted;

    public MovieProfileEntity(String profileId, String movieId) {
        this.profileId = profileId;
        this.movieId = movieId;
        this.accepted = true;
    }
}
