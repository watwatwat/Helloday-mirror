package me.noly.helloday.movie;

import com.uwetrottmann.tmdb2.entities.Movie;
import lombok.Data;

@Data
public class MovieResource {

    private static final String IMAGE_URL = "https://image.tmdb.org/t/p/original/";

    private Integer id;
    private String title;
    private String description;
    private String imageUrl;
    private String $raw;

    MovieResource(Movie movie) {
        id = movie.id;
        title = movie.title;
        description = movie.overview;
        imageUrl = (IMAGE_URL + movie.poster_path).replace("original//", "original/");
        $raw = movie.toString();
    }
}
