import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {Http} from "@angular/http";

declare var process;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  movie;

  constructor(public navCtrl: NavController, public http: Http) {
    http.get('/api/movies').subscribe((movie) => {
      this.movie = movie.json();
    });
  }
}
