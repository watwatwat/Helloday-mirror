#!/usr/bin/env bash
cd "$(dirname "$0")"
# --

# BUILD DEMO
cd helloday-ionic
ionic build --prod
cd ..
rm -rf helloday-api/src/main/resources/static/demo
cp -R helloday-ionic/www helloday-api/src/main/resources/static/demo

# DEPLOYMENT
./helloday-api/scripts/push